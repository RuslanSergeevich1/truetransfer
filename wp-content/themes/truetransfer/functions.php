<?php
/**
 * Twenty Sixteen functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://developer.wordpress.org/themes/advanced-topics/child-themes/
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

/**
 * Twenty Sixteen only works in WordPress 4.4 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.4-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'twentysixteen_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 *
	 * Create your own twentysixteen_setup() function to override in a child theme.
	 *
	 * @since Twenty Sixteen 1.0
	 */
	function twentysixteen_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/twentysixteen
		 * If you're building a theme based on Twenty Sixteen, use a find and replace
		 * to change 'twentysixteen' to the name of your theme in all the template files
		 */
		load_theme_textdomain( 'twentysixteen' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for custom logo.
		 *
		 *  @since Twenty Sixteen 1.2
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 240,
				'width'       => 240,
				'flex-height' => true,
			)
		);

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/reference/functions/add_theme_support/#post-thumbnails
		 */
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 1200, 9999 );

		// This theme uses wp_nav_menu() in two locations.
		register_nav_menus(
			array(
				'primary' => __( 'Primary Menu', 'twentysixteen' ),
				'social'  => __( 'Social Links Menu', 'twentysixteen' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		/*
		 * Enable support for Post Formats.
		 *
		 * See: https://codex.wordpress.org/Post_Formats
		 */
		add_theme_support(
			'post-formats',
			array(
				'aside',
				'image',
				'video',
				'quote',
				'link',
				'gallery',
				'status',
				'audio',
				'chat',
			)
		);

		/*
		 * This theme styles the visual editor to resemble the theme style,
		 * specifically font, colors, icons, and column width.
		 */
		add_editor_style( array( 'css/editor-style.css', twentysixteen_fonts_url() ) );

		// Load regular editor styles into the new block-based editor.
		add_theme_support( 'editor-styles' );

		// Load default block styles.
		add_theme_support( 'wp-block-styles' );

		// Add support for responsive embeds.
		add_theme_support( 'responsive-embeds' );

		// Add support for custom color scheme.
		add_theme_support(
			'editor-color-palette',
			array(
				array(
					'name'  => __( 'Dark Gray', 'twentysixteen' ),
					'slug'  => 'dark-gray',
					'color' => '#1a1a1a',
				),
				array(
					'name'  => __( 'Medium Gray', 'twentysixteen' ),
					'slug'  => 'medium-gray',
					'color' => '#686868',
				),
				array(
					'name'  => __( 'Light Gray', 'twentysixteen' ),
					'slug'  => 'light-gray',
					'color' => '#e5e5e5',
				),
				array(
					'name'  => __( 'White', 'twentysixteen' ),
					'slug'  => 'white',
					'color' => '#fff',
				),
				array(
					'name'  => __( 'Blue Gray', 'twentysixteen' ),
					'slug'  => 'blue-gray',
					'color' => '#4d545c',
				),
				array(
					'name'  => __( 'Bright Blue', 'twentysixteen' ),
					'slug'  => 'bright-blue',
					'color' => '#007acc',
				),
				array(
					'name'  => __( 'Light Blue', 'twentysixteen' ),
					'slug'  => 'light-blue',
					'color' => '#9adffd',
				),
				array(
					'name'  => __( 'Dark Brown', 'twentysixteen' ),
					'slug'  => 'dark-brown',
					'color' => '#402b30',
				),
				array(
					'name'  => __( 'Medium Brown', 'twentysixteen' ),
					'slug'  => 'medium-brown',
					'color' => '#774e24',
				),
				array(
					'name'  => __( 'Dark Red', 'twentysixteen' ),
					'slug'  => 'dark-red',
					'color' => '#640c1f',
				),
				array(
					'name'  => __( 'Bright Red', 'twentysixteen' ),
					'slug'  => 'bright-red',
					'color' => '#ff675f',
				),
				array(
					'name'  => __( 'Yellow', 'twentysixteen' ),
					'slug'  => 'yellow',
					'color' => '#ffef8e',
				),
			)
		);

		// Indicate widget sidebars can use selective refresh in the Customizer.
		add_theme_support( 'customize-selective-refresh-widgets' );
	}
endif; // twentysixteen_setup
add_action( 'after_setup_theme', 'twentysixteen_setup' );

/**
 * Sets the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'twentysixteen_content_width', 840 );
}
add_action( 'after_setup_theme', 'twentysixteen_content_width', 0 );

/**
 * Add preconnect for Google Fonts.
 *
 * @since Twenty Sixteen 1.6
 *
 * @param array  $urls           URLs to print for resource hints.
 * @param string $relation_type  The relation type the URLs are printed.
 * @return array $urls           URLs to print for resource hints.
 */
function twentysixteen_resource_hints( $urls, $relation_type ) {
	if ( wp_style_is( 'twentysixteen-fonts', 'queue' ) && 'preconnect' === $relation_type ) {
		$urls[] = array(
			'href' => 'https://fonts.gstatic.com',
			'crossorigin',
		);
	}

	return $urls;
}
add_filter( 'wp_resource_hints', 'twentysixteen_resource_hints', 10, 2 );

/**
 * Registers a widget area.
 *
 * @link https://developer.wordpress.org/reference/functions/register_sidebar/
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_widgets_init() {
	register_sidebar(
		array(
			'name'          => __( 'Sidebar', 'twentysixteen' ),
			'id'            => 'sidebar-1',
			'description'   => __( 'Add widgets here to appear in your sidebar.', 'twentysixteen' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

	register_sidebar(
		array(
			'name'          => __( 'Content Bottom 1', 'twentysixteen' ),
			'id'            => 'sidebar-2',
			'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'twentysixteen' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

	register_sidebar(
		array(
			'name'          => __( 'Content Bottom 2', 'twentysixteen' ),
			'id'            => 'sidebar-3',
			'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'twentysixteen' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'twentysixteen_widgets_init' );

if ( ! function_exists( 'twentysixteen_fonts_url' ) ) :
	/**
	 * Register Google fonts for Twenty Sixteen.
	 *
	 * Create your own twentysixteen_fonts_url() function to override in a child theme.
	 *
	 * @since Twenty Sixteen 1.0
	 *
	 * @return string Google fonts URL for the theme.
	 */
	function twentysixteen_fonts_url() {
		$fonts_url = '';
		$fonts     = array();
		$subsets   = 'latin,latin-ext';

		/* translators: If there are characters in your language that are not supported by Merriweather, translate this to 'off'. Do not translate into your own language. */
		if ( 'off' !== _x( 'on', 'Merriweather font: on or off', 'twentysixteen' ) ) {
			$fonts[] = 'Merriweather:400,700,900,400italic,700italic,900italic';
		}

		/* translators: If there are characters in your language that are not supported by Montserrat, translate this to 'off'. Do not translate into your own language. */
		if ( 'off' !== _x( 'on', 'Montserrat font: on or off', 'twentysixteen' ) ) {
			$fonts[] = 'Montserrat:400,700';
		}

		/* translators: If there are characters in your language that are not supported by Inconsolata, translate this to 'off'. Do not translate into your own language. */
		if ( 'off' !== _x( 'on', 'Inconsolata font: on or off', 'twentysixteen' ) ) {
			$fonts[] = 'Inconsolata:400';
		}

		if ( $fonts ) {
			$fonts_url = add_query_arg(
				array(
					'family' => urlencode( implode( '|', $fonts ) ),
					'subset' => urlencode( $subsets ),
				),
				'https://fonts.googleapis.com/css'
			);
		}

		return $fonts_url;
	}
endif;

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'twentysixteen_javascript_detection', 0 );

/**
 * Enqueues scripts and styles.
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'twentysixteen-fonts', twentysixteen_fonts_url(), array(), null );

	// Add Genericons, used in the main stylesheet.
	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.4.1' );

	// Theme stylesheet.
	wp_enqueue_style( 'twentysixteen-style', get_stylesheet_uri() );

	// Theme block stylesheet.
	wp_enqueue_style( 'twentysixteen-block-style', get_template_directory_uri() . '/css/blocks.css', array( 'twentysixteen-style' ), '20181230' );

	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'twentysixteen-ie', get_template_directory_uri() . '/css/ie.css', array( 'twentysixteen-style' ), '20160816' );
	wp_style_add_data( 'twentysixteen-ie', 'conditional', 'lt IE 10' );

	// Load the Internet Explorer 8 specific stylesheet.
	wp_enqueue_style( 'twentysixteen-ie8', get_template_directory_uri() . '/css/ie8.css', array( 'twentysixteen-style' ), '20160816' );
	wp_style_add_data( 'twentysixteen-ie8', 'conditional', 'lt IE 9' );

	// Load the Internet Explorer 7 specific stylesheet.
	wp_enqueue_style( 'twentysixteen-ie7', get_template_directory_uri() . '/css/ie7.css', array( 'twentysixteen-style' ), '20160816' );
	wp_style_add_data( 'twentysixteen-ie7', 'conditional', 'lt IE 8' );

	// Load the html5 shiv.
	wp_enqueue_script( 'twentysixteen-html5', get_template_directory_uri() . '/js/html5.js', array(), '3.7.3' );
	wp_script_add_data( 'twentysixteen-html5', 'conditional', 'lt IE 9' );

	wp_enqueue_script( 'twentysixteen-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20160816', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'twentysixteen-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20160816' );
	}

	wp_enqueue_script( 'twentysixteen-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20181230', true );

	wp_localize_script(
		'twentysixteen-script',
		'screenReaderText',
		array(
			'expand'   => __( 'expand child menu', 'twentysixteen' ),
			'collapse' => __( 'collapse child menu', 'twentysixteen' ),
		)
	);
}
add_action( 'wp_enqueue_scripts', 'twentysixteen_scripts' );

/**
 * Enqueue styles for the block-based editor.
 *
 * @since Twenty Sixteen 1.6
 */
function twentysixteen_block_editor_styles() {
	// Block styles.
	wp_enqueue_style( 'twentysixteen-block-editor-style', get_template_directory_uri() . '/css/editor-blocks.css', array(), '20181230' );
	// Add custom fonts.
	wp_enqueue_style( 'twentysixteen-fonts', twentysixteen_fonts_url(), array(), null );
}
add_action( 'enqueue_block_editor_assets', 'twentysixteen_block_editor_styles' );

/**
 * Adds custom classes to the array of body classes.
 *
 * @since Twenty Sixteen 1.0
 *
 * @param array $classes Classes for the body element.
 * @return array (Maybe) filtered body classes.
 */
function twentysixteen_body_classes( $classes ) {
	// Adds a class of custom-background-image to sites with a custom background image.
	if ( get_background_image() ) {
		$classes[] = 'custom-background-image';
	}

	// Adds a class of group-blog to sites with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of no-sidebar to sites without active sidebar.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'twentysixteen_body_classes' );

/**
 * Converts a HEX value to RGB.
 *
 * @since Twenty Sixteen 1.0
 *
 * @param string $color The original color, in 3- or 6-digit hexadecimal form.
 * @return array Array containing RGB (red, green, and blue) values for the given
 *               HEX code, empty array otherwise.
 */
function twentysixteen_hex2rgb( $color ) {
	$color = trim( $color, '#' );

	if ( strlen( $color ) === 3 ) {
		$r = hexdec( substr( $color, 0, 1 ) . substr( $color, 0, 1 ) );
		$g = hexdec( substr( $color, 1, 1 ) . substr( $color, 1, 1 ) );
		$b = hexdec( substr( $color, 2, 1 ) . substr( $color, 2, 1 ) );
	} elseif ( strlen( $color ) === 6 ) {
		$r = hexdec( substr( $color, 0, 2 ) );
		$g = hexdec( substr( $color, 2, 2 ) );
		$b = hexdec( substr( $color, 4, 2 ) );
	} else {
		return array();
	}

	return array(
		'red'   => $r,
		'green' => $g,
		'blue'  => $b,
	);
}

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images
 *
 * @since Twenty Sixteen 1.0
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function twentysixteen_content_image_sizes_attr( $sizes, $size ) {
	$width = $size[0];

	if ( 840 <= $width ) {
		$sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px';
	}

	if ( 'page' === get_post_type() ) {
		if ( 840 > $width ) {
			$sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
		}
	} else {
		if ( 840 > $width && 600 <= $width ) {
			$sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px';
		} elseif ( 600 > $width ) {
			$sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
		}
	}

	return $sizes;
}
add_filter( 'wp_calculate_image_sizes', 'twentysixteen_content_image_sizes_attr', 10, 2 );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails
 *
 * @since Twenty Sixteen 1.0
 *
 * @param array $attr Attributes for the image markup.
 * @param int   $attachment Image attachment ID.
 * @param array $size Registered image size or flat array of height and width dimensions.
 * @return array The filtered attributes for the image markup.
 */
function twentysixteen_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {
	if ( 'post-thumbnail' === $size ) {
		if ( is_active_sidebar( 'sidebar-1' ) ) {
			$attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 60vw, (max-width: 1362px) 62vw, 840px';
		} else {
			$attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 88vw, 1200px';
		}
	}
	return $attr;
}
add_filter( 'wp_get_attachment_image_attributes', 'twentysixteen_post_thumbnail_sizes_attr', 10, 3 );

/**
 * Modifies tag cloud widget arguments to display all tags in the same font size
 * and use list format for better accessibility.
 *
 * @since Twenty Sixteen 1.1
 *
 * @param array $args Arguments for tag cloud widget.
 * @return array The filtered arguments for tag cloud widget.
 */
function twentysixteen_widget_tag_cloud_args( $args ) {
	$args['largest']  = 1;
	$args['smallest'] = 1;
	$args['unit']     = 'em';
	$args['format']   = 'list';

	return $args;
}
add_filter( 'widget_tag_cloud_args', 'twentysixteen_widget_tag_cloud_args' );

function dequeue_my_css() {
    wp_dequeue_style('twentysixteen-style');
    wp_deregister_style('twentysixteen-style');
}
add_action('wp_enqueue_scripts','dequeue_my_css');

if ( function_exists( 'add_image_size' ) ) {
    add_image_size( 'partners-thumb', 190, 99999 ); // Кадрирование изображения
    add_image_size( 'gallery-thumb', 360, 270, true ); // Кадрирование изображения
}

add_filter( 'image_size_names_choose', 'my_custom_sizes' );

function my_custom_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'partners-thumb' => 'Наши партнеры миниатюра',
        'gallery-thumb' => 'Галлерея миниатюра',
    ) );
}


/*
* Creating a function to create our CPT
*/

function custom_post_type() {

// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Отзывы', 'Post Type General Name', 'truetransfer' ),
        'singular_name'       => _x( 'Отзыв', 'Post Type Singular Name', 'truetransfer' ),
        'menu_name'           => __( 'Отзывы', 'truetransfer' ),
//        'parent_item_colon'   => __( 'Parent Movie', 'truetransfer' ),
        'all_items'           => __( 'Все Отзывы', 'truetransfer' ),
        'view_item'           => __( 'Посмотреть Отзыв', 'truetransfer' ),
        'add_new_item'        => __( 'Добавить Новый Отзыв', 'truetransfer' ),
        'add_new'             => __( 'Добавить Отзыв', 'truetransfer' ),
        'edit_item'           => __( 'Редактировать Отзыв', 'truetransfer' ),
        'update_item'         => __( 'Отзыв Отзыв', 'truetransfer' ),
        'search_items'        => __( 'Найти Отзыв', 'truetransfer' ),
        'not_found'           => __( 'Не найдено', 'truetransfer' ),
        'not_found_in_trash'  => __( 'Не найдено', 'truetransfer' ),
    );

// Set other options for Custom Post Type

    $args = array(
        'label'               => __( 'отзывы', 'truetransfer' ),
        'description'         => __( 'Отзывы', 'truetransfer' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array(
            'title',
            'editor',
            'excerpt',
            'author',
            'thumbnail',
            'comments',
            'revisions',
            'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy.
        'taxonomies'          => array( 'genres' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
        'menu_icon'   => 'dashicons-testimonial',
    );

    // Registering your Custom Post Type
    register_post_type( 'reviews', $args );

}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not
* unnecessarily executed.
*/

add_action( 'init', 'custom_post_type', 0 );


function partners_post_type() {

// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Партнеры', 'Post Type General Name', 'truetransfer' ),
        'singular_name'       => _x( 'Партнер', 'Post Type Singular Name', 'truetransfer' ),
        'menu_name'           => __( 'Партнеры', 'truetransfer' ),
//        'parent_item_colon'   => __( 'Parent Movie', 'truetransfer' ),
        'all_items'           => __( 'Все Партнеры', 'truetransfer' ),
        'view_item'           => __( 'Посмотреть Партнера', 'truetransfer' ),
        'add_new_item'        => __( 'Добавить Нового Партнера', 'truetransfer' ),
        'add_new'             => __( 'Добавить Партнера', 'truetransfer' ),
        'edit_item'           => __( 'Редактировать Партнеры', 'truetransfer' ),
        'update_item'         => __( 'Обновить Партнера', 'truetransfer' ),
        'search_items'        => __( 'Найти Партнера', 'truetransfer' ),
        'not_found'           => __( 'Не найдено', 'truetransfer' ),
        'not_found_in_trash'  => __( 'Не найдено', 'truetransfer' ),
    );

// Set other options for Custom Post Type

    $args = array(
        'label'               => __( 'партнеры', 'truetransfer' ),
        'description'         => __( 'Партнеры', 'truetransfer' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array(
            'title',
            'editor',
            'excerpt',
            'author',
            'thumbnail',
            'comments',
            'revisions',
            'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy.
        'taxonomies'          => array( 'genres' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
        'menu_icon'   => 'dashicons-groups',
    );

    // Registering your Custom Post Type
    register_post_type( 'partners', $args );

}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not
* unnecessarily executed.
*/

add_action( 'init', 'partners_post_type', 0 );


function add_theme_menu_item()
{
    add_menu_page("Настройки сайта", "Настройки сайта", "manage_options", "theme-panel", "theme_settings_page", null, 99);
}

add_action("admin_menu", "add_theme_menu_item");


function theme_settings_page()
{
    ?>
    <div class="wrap custom-style">
        <h1>Настройки Сайта</h1>
        <div class="notice notice-success is-dismissible">
            <p><strong>Настройки сохранены</strong></p>
        </div>
        <form method="post" action="options.php">
            <?php settings_fields("section"); ?>
            <?php do_settings_sections("theme-options"); ?>
            <p class="important" style="color: red"><strong>Важно! Чтобы получить данные из таблицы цен для списка формы заказа надо в URL добавить #showlist</strong></p>
            <p class="important">Например: <a target="_blank" href="https://<?php echo $_SERVER['HTTP_HOST'];?>/czeny/#showlist">/czeny/#showlist</a></p>
            <?php submit_button(); ?>
        </form>
    </div>
    <?php
}

add_action('admin_head', 'custom_style');

function custom_style() {
    echo '<style>
    .custom-style td,
    .custom-style th {
      padding: 5px 0;
    }
    .custom-style .input {
        width: 300px;
    }
    .important {
        font-size: 18px;
        line-height: 18px;
        margin: 5px 0;
    }
  </style>';
}

function display_vk_element()
{
    ?>
    <input type="text" name="vk_url" class="input" id="vk_url" value="<?php echo get_option('vk_url'); ?>" />
    <?php
}

function display_facebook_element()
{
    ?>
    <input type="text" name="facebook_url" class="input" id="facebook_url" value="<?php echo get_option('facebook_url'); ?>" />
    <?php
}

function display_phone_1_element()
{
    ?>
    <input type="text" name="phone_1" class="input" id="phone_1" value="<?php echo get_option('phone_1'); ?>" />
    <?php
}

function display_phone_2_element()
{
    ?>
    <input type="text" name="phone_2" class="input" id="phone_2" value="<?php echo get_option('phone_2'); ?>" />
    <?php
}

function display_phone_3_element()
{
    ?>
    <input type="text" name="phone_3" class="input" id="phone_3" value="<?php echo get_option('phone_3'); ?>" />
    <?php
}

function display_email_element()
{
    ?>
    <input type="text" name="email" class="input" id="email" value="<?php echo get_option('email'); ?>" />
    <?php
}

function display_work_element()
{
    ?>
    <textarea id="work" name="work"
              rows="5" cols="50"><?php echo get_option('work'); ?></textarea>
    <?php
}

function display_copyright_element()
{
    ?>
    <textarea id="copyright" name="copyright"
              rows="5" cols="50"><?php echo get_option('copyright'); ?></textarea>
    <?php
}

function display_privacy_element()
{
    ?>
    <textarea id="privacy" name="privacy"
              rows="5" cols="50"><?php echo get_option('privacy'); ?></textarea>
    <?php
}


function display_custom_footer_code_element()
{
    ?>
    <textarea id="custom_footer_code" name="custom_footer_code"
              rows="5" cols="50"><?php echo get_option('custom_footer_code'); ?></textarea>
    <?php
}

function display_custom_head_code_element()
{
    ?>
    <textarea id="custom_head_code" name="custom_head_code"
              rows="5" cols="50"><?php echo get_option('custom_head_code'); ?></textarea>
    <?php
}

function display_header_text_element()
{
    ?>
    <textarea id="head_text" name="head_text"
              rows="5" cols="50"><?php echo get_option('head_text'); ?></textarea>
    <?php
}

function display_show_call_button()
{
    ?>
    <input type="checkbox" name="show_call_button" class="checkboxes" id="show_call_button" value="1"<?php checked( get_option('show_call_button') ); ?> />
    <?php
}

function display_header_button_text_element()
{
    ?>
    <input type="text" name="header_button_text" class="input" id="header_button_text" value="<?php echo get_option('header_button_text'); ?>" />
    <?php
}

function display_header_button_link_element()
{
    ?>
    <input type="text" name="header_button_link" class="input" id="header_button_link" value="<?php echo get_option('header_button_link'); ?>" />
    <?php
}

function get_price_page_content()
{
    ?>
    <input type="text" name="id_price_page" class="input" id="id_price_page" value="<?php echo get_option('id_price_page'); ?>" />
    <?php
}

function show_price_page_content()
{
    ?>
    <input type="checkbox" name="show_price" class="checkboxes" id="show_price" value="1"<?php checked( get_option('show_price') ); ?> />
    <?php
}

function show_price_index_content()
{
    ?>
    <input type="checkbox" name="show_price_index" class="checkboxes" id="show_price_index" value="1"<?php checked( get_option('show_price_index') ); ?> />
    <?php
}

function exclude_price_page_content()
{
    ?>
    <input type="text" name="exclude_price" class="input" id="exclude_price" value="<?php echo get_option('exclude_price'); ?>" />
    <?php
}

function ya_review_widget()
{
    ?>
    <input type="checkbox" name="show_ya_review" class="checkboxes" id="show_ya_review" value="1"<?php checked( get_option('show_ya_review') ); ?> />
    <?php
}

function display_theme_panel_fields()
{
    add_settings_section("section", "Все настройки", null, "theme-options");

    add_settings_field("twitter_url", "Ссылка Вконтакте", "display_vk_element", "theme-options", "section");
    add_settings_field("facebook_url", "Ссылка Facebook", "display_facebook_element", "theme-options", "section");
    add_settings_field("phone_1", "Номер телефона", "display_phone_1_element", "theme-options", "section");
    add_settings_field("phone_2", "Номер телефона", "display_phone_2_element", "theme-options", "section");
    add_settings_field("phone_3", "Номер телефона", "display_phone_3_element", "theme-options", "section");
    add_settings_field("email", "Почта", "display_email_element", "theme-options", "section");
    add_settings_field("header_button_text", "Текст кнопки", "display_header_button_text_element", "theme-options", "section");
    add_settings_field("header_button_link", "Ссылка кнопки", "display_header_button_link_element", "theme-options", "section");
    add_settings_field("head_text", "Слоган", "display_header_text_element", "theme-options", "section");
    add_settings_field("work", "Часы работы", "display_work_element", "theme-options", "section");
    add_settings_field("copyright", "Копирайт", "display_copyright_element", "theme-options", "section");
    add_settings_field("privacy", "Политика конфиденциальности", "display_privacy_element", "theme-options", "section");
    add_settings_field("custom_head_code", "Код на сайт в head", "display_custom_head_code_element", "theme-options", "section");
    add_settings_field("custom_footer_code", "Код на сайт в footer", "display_custom_footer_code_element", "theme-options", "section");
    add_settings_field("show_call_button", "Показывать кнопку обратной связи", "display_show_call_button", "theme-options", "section");
    add_settings_field("id_price_page", "ID страницы цен", "get_price_page_content", "theme-options", "section");
    add_settings_field("show_ya_review", "Показать виджет яндекс отзывы", "ya_review_widget", "theme-options", "section");
    add_settings_field("show_price", "Показать список направлений", "show_price_page_content", "theme-options", "section");
    add_settings_field("show_price_index", "Показать индексы направлений", "show_price_index_content", "theme-options", "section");
    add_settings_field("exclude_price", "Исключить направления(номера через запятную)", "exclude_price_page_content", "theme-options", "section");

    register_setting("section", "vk_url");
    register_setting("section", "facebook_url");
    register_setting("section", "phone_1");
    register_setting("section", "phone_2");
    register_setting("section", "phone_3");
    register_setting("section", "email");
    register_setting("section", "head_text");
    register_setting("section", "header_button_text");
    register_setting("section", "header_button_link");
    register_setting("section", "copyright");
    register_setting("section", "privacy");
    register_setting("section", "work");
    register_setting("section", "custom_footer_code");
    register_setting("section", "custom_head_code");
    register_setting("section", "show_call_button");
    register_setting("section", "id_price_page");
    register_setting("section", "show_ya_review");
    register_setting("section", "show_price");
    register_setting("section", "show_price_index");
    register_setting("section", "exclude_price");
}

add_action("admin_init", "display_theme_panel_fields");

add_filter( 'excerpt_length', function(){
    return 18;
} );

add_filter('excerpt_more', function($more) {
    return '...';
});

add_filter( 'get_the_archive_title', function ($title) {
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    } elseif ( is_tag() ) {
        $title = single_tag_title( '', false );
    } elseif ( is_author() ) {
        $title = '<span class="vcard">' . get_the_author() . '</span>' ;
    } elseif ( is_tax() ) { //for custom post types
        $title = sprintf( __( '%1$s' ), single_term_title( '', false ) );
    }
    return $title;
});


add_filter('navigation_markup_template', 'my_navigation_template', 10, 2 );
function my_navigation_template( $template, $class ){
    return '
	<nav class="navigation %1$s" role="navigation">
		<div class="nav-links">%3$s</div>
	</nav>    
	';
}

// выводим пагинацию
the_posts_pagination( array(
    'end_size' => 2,
) );


function check_mobile_device()
{
    $mobile_agent_array = array('ipad', 'iphone', 'android', 'pocket', 'palm', 'windows ce', 'windowsce', 'cellphone', 'opera mobi', 'ipod', 'small', 'sharp', 'sonyericsson', 'symbian', 'opera mini', 'nokia', 'htc_', 'samsung', 'motorola', 'smartphone', 'blackberry', 'playstation portable', 'tablet browser');
    $agent = strtolower($_SERVER['HTTP_USER_AGENT']);
    foreach ($mobile_agent_array as $value) {
        if (strpos($agent, $value) !== false) return true;
    };
    return false;
}

add_action( 'wp_ajax_getPostContent', 'getPostContent' );

function getPostContent() {
    $post_id = get_option('id_price_page');
    $show_content = get_option('show_price');
    $show_price_index = get_option('show_price_index');
    $exclude_price = get_option('exclude_price');
    if($post_id) {
        $post = get_post($post_id);
        $data = apply_filters('the_content', $post->post_content);
        echo json_encode(array('success' => true, 'data' => $data, 'show' => $show_content, 'show_price_index' => $show_price_index, 'exclude' => $exclude_price));
    }
    wp_die();
}

function getPostContentFooter() {
    $post_id = get_option('id_price_page');
    if($post_id) {
        $post = get_post($post_id);
        $data = apply_filters('the_content', $post->post_content);
        return $data;
    }
    wp_die();
}
