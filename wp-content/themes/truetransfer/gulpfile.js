const gulp = require('gulp');
const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');
const rename = require('gulp-rename');
const minify = require('gulp-minify');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const pipeline = require('readable-stream').pipeline;
const strip = require('gulp-strip-comments');
const stripCssComments = require('gulp-strip-css-comments');

gulp.task('sass', () => {
    return gulp.src([
        'assets/sass/**/*.scss',
    ])
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCSS({compatibility: 'ie8', debug: true}, (details) => {
            console.log(`${details.name}: ${details.stats.originalSize}`);
            console.log(`${details.name}: ${details.stats.minifiedSize}`);
        }))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('./dist/css'));
});

gulp.task('css', () => {
    return gulp.src([
        'node_modules/animate.css/animate.min.css',
        'node_modules/jquery-colorbox/example3/colorbox.css',
        'node_modules/slick-carousel/slick/slick.css',
        'node_modules/slick-carousel/slick/slick-theme.css',
        'node_modules/@fortawesome/fontawesome-free/css/all.min.css'
    ])
        .pipe(cleanCSS({compatibility: 'ie8', debug: true}, (details) => {
            console.log(`${details.name}: ${details.stats.originalSize}`);
            console.log(`${details.name}: ${details.stats.minifiedSize}`);
        }))
        .pipe(concat('plugins.min.css'))
        .pipe(stripCssComments({preserve: false}))
        .pipe(gulp.dest('./dist/css'));
});

gulp.task('js', function() {
    return gulp.src([
        'node_modules/wow.js/dist/wow.min.js',
        'node_modules/jquery-colorbox/jquery.colorbox-min.js',
        'node_modules/slick-carousel/slick/slick.min.js',
        'assets/js/custom.js'
    ])
        .pipe(uglify())
        .pipe(concat('scripts.min.js'))
        .pipe(strip())
        .pipe(gulp.dest("./dist/js"));
});

gulp.task('watch', () => {
    gulp.watch('assets/sass/**/*.scss', (done) => {
        gulp.series(['sass'])(done);
    });
    gulp.watch('/node_modules/wow.js/css/libs/animate.css', (done) => {
        gulp.series(['css'])(done);
    });
    gulp.watch(['assets/js/**/*.js'], (done) => {
        gulp.series(['js'])(done);
    });
});

gulp.task('default', gulp.series(['sass', 'css', 'js']));