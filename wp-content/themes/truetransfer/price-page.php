<?php
/**
 * Template Name: Цены
 *
 * The template for displaying Price
 */

get_header(); ?>
    <section class="page container">
        <div class="page__content">
            <h1 class="page__title title"><?php the_title();?></h1>
            <div class="page__text check-list">
                <?php the_content();?>
            </div>
        </div>
    </section>
    <?php $fields = get_fields();?>
    <?php if($fields['zakazhite_transfer']): ?>
        <section class="order container-vertical"
            <?php if($fields['zakazhite_transfer']['izobrazhenie']): ?>
                style="background-image: url(<?php echo $fields['zakazhite_transfer']['izobrazhenie'];?>);
                        background-size: cover;
                        background-attachment: fixed;
                        background-position: center;"
            <?php endif; ?>
        >
            <div class="order__content">
                <?php if($fields['zakazhite_transfer']['tekst']): ?>
                    <div class="order__content-text">
                        <?php echo $fields['zakazhite_transfer']['tekst'];?>
                    </div>
                <?php endif; ?>
                <?php if($fields['zakazhite_transfer']['nazvanie_knopki'] &&
                    $fields['zakazhite_transfer']['ssylka']): ?>
                    <div class="order__button">
                        <a href="<?php echo $fields['zakazhite_transfer']['ssylka']?>"
                           class="button-green scale no-margin">
                            <?php echo $fields['zakazhite_transfer']['nazvanie_knopki']?>
                        </a>
                    </div>
                <?php endif; ?>
            </div>
        </section>
    <?php endif; ?>
    <?php if($fields['ostalis_voprosy']): ?>
        <section class="questions container__fullwidth">
            <div class="questions__content">
                <?php if($fields['ostalis_voprosy']['tekst']): ?>
                    <div class="questions__content-text">
                        <?php echo $fields['ostalis_voprosy']['tekst'];?>
                    </div>
                <?php endif; ?>
                <?php if($fields['ostalis_voprosy']['nomer_telefona']): ?>
                    <div class="questions__content-phone">
                        <a class="questions__content-phone-link scale"
                           href="tel:<?php echo $fields['ostalis_voprosy']['nomer_telefona'];?>">
                            <?php echo $fields['ostalis_voprosy']['nomer_telefona'];?>
                        </a>
                    </div>
                <?php endif; ?>
            </div>
        </section>
    <?php endif; ?>
<?php get_footer(); ?>