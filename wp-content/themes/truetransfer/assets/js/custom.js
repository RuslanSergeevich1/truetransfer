jQuery( document ).ready(function() {
    var footerHeight = jQuery('footer').height();
    var headerHeight = jQuery('header').height();
    var browserHeight = jQuery( window ).height();
    var selectedCar = jQuery('span[data-name="select-avto"] select').find('option:selected').val();
    // console.log(selectedCar);
    var selectedPriceFrom = jQuery('.transfer-from').find('input:checked').val();
    var selectedPriceTo = jQuery('.transfer-to').find('option:selected').val();
    var priceTableLink = jQuery('td a');

    priceTableLink.each(function() {
        var title = jQuery(this).attr('title');
        jQuery(this).attr('data-title', title);
        jQuery(this).removeAttr( 'title' );
    });
    priceTableLink.on('click', function( event ) {
        event.preventDefault();
    });
    priceTableLink.append('<i class="fas fa-regular fa-info distance-question"></i>');
    priceTableLink.hover(
        function() {
            var title = jQuery(this).attr('data-title');
            var width = jQuery(this).width();
            var tooltipTitle = 'Расстояние неизвестно';
            var tooltipClass = 'tooltip-box undefined';
            if (title) {
                tooltipTitle = title;
                tooltipClass = 'tooltip-box';
            }
            jQuery('<div/>', {
                text: tooltipTitle,
                class: tooltipClass,
            }).appendTo(this);
            jQuery('div.tooltip-box').css('left', width);
        }, function() {
            jQuery(document).find('div.tooltip-box').remove();
        }
    );

    jQuery('.page').css('min-height', browserHeight - headerHeight - footerHeight);

    jQuery( window ).resize(function() {
        var browserHeight = jQuery( window ).height();
        jQuery('.page').css('min-height', browserHeight - headerHeight - footerHeight);
    });

    jQuery('.transfer-to .select2-selection__placeholder').text('Выберите направление');
    jQuery(function () {
        var hash = window.location.hash;
        if (hash.substring(1) === 'showlist') {
            jQuery.ajax({
                type: 'POST',
                url: '/wp-admin/admin-ajax.php',
                data: {
                    'action': 'getPostContent',
                    'dataType': 'JSON'//this is the name of the AJAX method called in WordPress
                }, success: function (result) {
                    var res = JSON.parse(result);
                    // console.log(res.data);
                    // jQuery('.tablePrice').append(res.data);
                    getPriceFromContent('.tablePrice', res.show, res.exclude, res.show_price_index );
                    // getPriceFromContent(result);
                    getTotalPrice(priceArr, selectedCar, selectedPriceFrom, selectedPriceTo)
                },
                error: function (result) {
                    // console.log(result);
                }
            });
        }
    });

    var priceArr = [];
    var priceArrT = [];

    function getPriceFromContent(tag, show, exclude, show_price_index) {
        if(exclude) {
            var array = exclude.split(', ').map(Number);
        }

        var checkIndex = !!show_price_index;
        if(show && show === '1'){
            jQuery('body table').append('<div class="excluded_ids"></div>');
        }
        jQuery(tag + ' table tr').each( (tr_idx,tr) => {
            if (jQuery.inArray(tr_idx, array) <= -1) {
                var text = [];
                var delimeter = '';
                var textSelect = '';
                jQuery(tr).children('td').each((td_idx, td) => {
                    // td_idx это столбец (0 = название || 1 = Легковое  авто || 2 = Микроавтобус)
                    var carPrice = false;
                    var busPrice = false;
                    if (td_idx === 0 || (td_idx === 1 && carPrice) || (td_idx === 2 && busPrice)) {
                        if (td_idx !== 0) {
                            delimeter = ' | ';
                        }
                        textSelect = textSelect + delimeter + jQuery(td).text();
                    }
                    text[td_idx] = jQuery(td).text();
                });

                priceArr.push(text);
                if(show && show === '1'){
                    jQuery('body table').append((checkIndex ? tr_idx + ' - ' : '') + '"' + textSelect + '" <br/>');
                    let text = ((checkIndex ? tr_idx + ' - ' : '') + '"' + textSelect + '"');
                    priceArrT.push(textSelect);
                    if(text.includes('"\n')){
                        jQuery('.excluded_ids').append(tr_idx+', ');
                    }

                }
            }
        });
        if(show && show === '1') {
            if (priceArrT && priceArrT.length > 0) {
                jQuery('body table').append('<br/><span style="color: green">Готовый Список в форму:</span><br/><br/>');
                priceArrT.sort();
                jQuery.each(priceArrT, function (index, value) {
                    jQuery('body table').append('"' + value + '" <br/>');
                });
            }
        }
    }

    function getTotalPrice (priceArr, selectedCar, from, to) {
        // console.log(selectedCar);
        // console.log(from);
        // console.log(to);
        // console.log(priceArr);
        jQuery('body .total-car-price').empty();

        let car = 1;

        if(selectedCar === 'Микроавтобус'){
            car = 2;
        }
        priceArr.forEach( (val, index) => {
            if (val[0] === to) {
                // console.log(road);
                // console.log(car);
                if(val[car] && val[car].length > 1){
                    jQuery('body .total-car-price').append('<p>Стоимость трансфера - <span style="color:red">' + val[car] + ' р.</span></p>');
                }
            }
        });
    }

    jQuery('span[data-name="select-avto"] select').on('change', function (e) {
        // console.log(this.value);
        if(priceArr.length === 0){
            getPriceFromContent('.tablePrice', null, null, null );
        }
        selectedCar = this.value;
        getTotalPrice(priceArr, selectedCar, selectedPriceFrom, selectedPriceTo)
    });


    jQuery('span[data-name="transfer-from"] select').on('change', function (e) {
        // console.log(this.value);
        if(priceArr.length === 0){
            getPriceFromContent('.tablePrice', null, null, null );
        }
        selectedPriceFrom = this.value;
        getTotalPrice(priceArr, selectedCar, selectedPriceFrom, selectedPriceTo)
    });

    jQuery('.transfer-to select').on('change', function (e) {
        if(priceArr.length === 0){
            getPriceFromContent('.tablePrice', null, null, null );
        }
        // console.log(this.value);
        selectedPriceTo = this.value;
        getTotalPrice(priceArr, selectedCar, selectedPriceFrom, selectedPriceTo)
    });

    // console.log(selectedCar);
    // console.log(selectedPriceFrom);
    // console.log(selectedPriceTo);


    var btn = jQuery('#button');

    jQuery(window).scroll(function() {
        if (jQuery(window).scrollTop() > 300) {
            btn.addClass('show');
        } else {
            btn.removeClass('show');
        }
    });

    btn.on('click', function(e) {
        e.preventDefault();
        jQuery('html, body').animate({scrollTop:0}, '300');
    });

    var hamburger = jQuery('#hamburger-icon');
    hamburger.click(function() {
        hamburger.toggleClass('active');
        return false;
    });

    jQuery(".slider").slick({
        dots: true,
        infinite: true,
        centerMode: true,
        slidesToShow: 5,
        slidesToScroll: 3,
        arrows: false,
        autoplay: true,
        mobileFirst: true,
        centerPadding: '0px 20px',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 5
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 320,
                settings: {
                    slidesToShow: 2
                }
            }
        ]

    });
});
var wow = new WOW(
    {
        boxClass:     'wow',      // animated element css class (default is wow)
        animateClass: 'animated', // animation css class (default is animated)
        offset:       100,          // distance to the element when triggering the animation (default is 0)
        mobile:       true,       // trigger animations on mobile devices (default is true)
        live:         true,       // act on asynchronously loaded content (default is true)
        callback:     function(box) {
            // the callback is fired every time an animation is started
            // the argument that is passed in is the DOM node being animated
        },
        scrollContainer: null,    // optional scroll container selector, otherwise use window,
        resetAnimation: true,     // reset animation on end (default is true)
    }
);
wow.init();