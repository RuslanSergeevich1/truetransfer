<?php
/**
 * The template for displaying reviews pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

get_header(); ?>
<section class="review container">
    <div class="review__content">
        <h1 class="review__title title"><?php post_type_archive_title();?></h1>
        <?php $reviews = get_page_by_path('reviews');
            $content = apply_filters( 'the_content', $reviews->post_content );
            echo $content;
        ?>

        <?php if ( have_posts() ) : ?>
        <?php
			// Start the Loop.
			while ( have_posts() ) : the_post();?>
                <div class="review__item">
                    <div class="review__item-img" style="background-image: url('<?php echo get_the_post_thumbnail_url('', 'medium_large');?>')"></div>
                    <div class="review__item-content">
                        <div class="review__item-title">
                            <?php the_title();?>
                            <span class="date">
                                <?php echo get_the_date('n-j-Y');?>
                            </span>
                        </div>
                        <div class="review__item-text">
                            <?php the_content();?>
                        </div>
                    </div>
                </div>
			<?php endwhile; ?>
			    <div class="pagination">
                    <?php the_posts_pagination(); ?>
                </div>
        <?php else :
            get_template_part( 'template-parts/content', 'none' );

        endif;
        ?>
    </div>
</section>
<?php get_footer(); ?>
