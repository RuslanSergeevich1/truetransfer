<?php
/**
 * The template for displaying 404 pages (not found)
 */
    get_header(); ?>
    <section class="page container">
        <div class="page__content">
            <h1 class="page__title title">
              Упс! Страница не найдена :(
            </h1>
        </div>
    </section>
<?php get_footer(); ?>