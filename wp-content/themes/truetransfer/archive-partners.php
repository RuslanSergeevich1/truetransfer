<?php
/**
 * The template for displaying reviews pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

get_header(); ?>
<section class="partners-page container">
    <div class="partners-page__content">
        <h1 class="partners-page__title title"><?php post_type_archive_title();?></h1>
        <?php if ( have_posts() ) : ?>
        <div class="partners-page__list">
        <?php
			// Start the Loop.
            $args = array( 'posts_per_page' => 99, 'post_type' => 'partners' );
            query_posts( $args );
			while ( have_posts() ) : the_post();?>
                <?php if(has_post_thumbnail()):?>
                    <div class="partners-page__item">
                        <div class="partners-page__item-img">
                            <?php echo get_the_post_thumbnail('', 'medium'); ?>
                        </div>
                    </div>
                <?php endif; ?>
			<?php endwhile;
			wp_reset_query();?>
        </div>
        <?php else :
            get_template_part( 'template-parts/content', 'none' );
        endif;
        ?>
    </div>
</section>
<?php get_footer(); ?>
