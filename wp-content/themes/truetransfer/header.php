<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php echo esc_url( get_bloginfo( 'pingback_url' ) ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/dist/css/style.min.css?v=1.3" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/dist/css/plugins.min.css?v=1.2" />
    <?php echo get_option('custom_head_code')?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<div id="page" class="site">
	<div class="site-inner">
      <header class="header">
    <div class="header__info">
        <div class="header__info-slogan">
            <a href="/"><?php echo get_option('head_text')?></a>
        </div>

        <div class="header__info-phones">
            <?php if(get_option('phone_1')) : ?>
            <?php $phone = str_replace(' ', '', get_option('phone_1'));?>
            <div class="header__info-phones-icons">
                <?php if(check_mobile_device()) :?>
                    <a title="Viber" href="viber://chat?number=<?php echo str_replace('+', '', $phone);?>">
                <?php else : ?>
                    <a title="Viber" href="viber://chat?number=<?php echo $phone;?>">
                <?php endif; ?>
                        <i class="fab fa-viber fa-2x"></i>
                    </a>
                <a title="Telegram" target="_blank" href="https://t.me/<?php echo $phone;?>">
                    <i class="fab fa-telegram-plane fa-2x"></i>
                </a>
                <a title="Whatsapp" target="_blank" href="https://wa.me/<?php echo str_replace('+', '', $phone);?>">
                    <i class="fab fa-whatsapp fa-2x"></i>
                </a>
            </div>
            <?php endif;?>
            <div class="header__info-phones-items">
            <?php if(get_option('phone_1')) :?>
                <div class="header__info-phones-first">
                    <a class="header__info-phones-link" href="tel:<?php echo get_option('phone_1')?>">
                        <?php echo get_option('phone_1')?>
                    </a>
                </div>
            <?php endif; ?>
            <?php if(get_option('phone_2')) :?>
                <div class="header__info-phones-second">
                    <a class="header__info-phones-link" href="tel:<?php echo get_option('phone_2')?>">
                        <?php echo get_option('phone_2')?>
                    </a>
                </div>
            <?php endif;?>
            <?php if(get_option('phone_2')) :?>
                <div class="header__info-phones-third">
                    <a class="header__info-phones-link" href="tel:<?php echo get_option('phone_3')?>">
                        <?php echo get_option('phone_3')?>
                    </a>
                </div>
            <?php endif;?>
            </div>
        </div>
        <div class="header__info-text">
            <span class="pulse-text"><?php echo get_option('work')?></span>
            <?php if(get_option('header_button_text')) :?>
                <a href="<?php echo get_option('header_button_link')?>"
                   class="button-green violet header__info-button">
                    <?php echo get_option('header_button_text')?>
                </a>
            <?php endif;?>
<!--            <a href="#"-->
<!--               class="button-green header__info-button">-->
<!--              --><?php //echo get_option('header_button_text')?>
<!--            </a>-->
        </div>
        <button id="menu-toggle" class="menu-toggle">
            <i class="fas fa-bars fa-2x"></i>
        </button>
    </div>
    <div class="header__menu">
        <?php if ( has_nav_menu( 'primary' ) ) : ?>
            <div id="nav">
                <?php if ( has_nav_menu( 'primary' ) ) : ?>
                    <nav>
                        <?php
                        wp_nav_menu(
                            array(
                                'theme_location' => 'primary',
                                'menu_class' => 'primary-menu',
                            )
                        );
                        ?>
                    </nav><!-- .main-navigation -->
                <?php endif; ?>
            </div><!-- .site-header-menu -->
        <?php endif; ?>
    </div>
</header>
		<div id="content" class="site-content">
