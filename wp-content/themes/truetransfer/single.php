<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
<section class="archive-page container">
    <div class="archive-page__content">
        <?php while ( have_posts() ) :
			the_post();?>
        <h1 class="archive-page__title title"><?php the_title();?></h1>
        <?php truetransfer_post_thumbnail(); ?>
        <?php the_content();?>
        <?php endwhile; ?>
    </div>
</section>
<?php get_footer(); ?>