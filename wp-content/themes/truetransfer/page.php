<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 */

get_header(); ?>
    <section class="page container">
        <div class="page__content">
            <h1 class="page__title title"><?php the_title();?></h1>
            <?php if (get_field( 'widget_button' )): ?>
            <span>Введите сумму для оплаты</span>
            <div id="widgetContainer" style="background-color: #dadad2"></div>
            <div class="rncb-widget-form">
                <input class="rncb-widget-field" placeholder="0 руб." type="text" name="fld_amount" id="fld_amount" value="" />
                <div class="rncb-form-element">
                    <button name="button" type="button" id="initWidget" aria-busy="false" class="custom-button button-green violet scale">
                        <span>Оплатить сейчас</span>
                    </button>
                </div>
                <div class="rncb-form-element">
                    <button name="button" type="button" id="continueButton" aria-busy="false" style="display: none">
                        <span></span>
                    </button>
                </div>
            </div>
            <?php endif; ?>
            <div class="page__text">
                <?php the_content();?>
            </div>
        </div>
    </section>
<?php get_footer(); ?>