<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

get_header(); ?>

<section class="archive-page container">
    <div class="archive-page__content">
        <h1 class="archive-page__title title"><?php the_archive_title();?></h1>
        <?php if ( have_posts() ) : ?>
            <div class="archive-page__list">
                <?php
                // Start the Loop.
                while ( have_posts() ) : the_post();?>
                    <?php if(has_post_thumbnail()):?>
                        <div class="archive-page__item wow fadeIn" data-wow-delay="0.1s">
                            <div class="archive-page__item-img">
                                <?php echo get_the_post_thumbnail('', 'medium'); ?>
                            </div>
                            <div class="archive-page__item-text">
                                <h2 class="archive-page__item-title">
                                    <?php the_title(); ?>
                                </h2>
                                <?php the_excerpt(); ?>
                                <div class="button">
                                    <a class="button-green scale no-margin right"
                                       href="<?php the_permalink(); ?>">Подробнее...</a>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endwhile; ?>
            </div>
            <div class="pagination">
                <?php the_posts_pagination(); ?>
            </div>
        <?php else :
            get_template_part( 'template-parts/content', 'none' );
        endif;
        ?>
    </div>
</section>
<?php get_footer(); ?>
