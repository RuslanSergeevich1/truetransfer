<?php
/**
 * Template Name: Homepage
 *
 * The template for displaying homepage
 */

get_header(); ?>
<div class="homepage">
    <?php $fields = get_fields(); ?>
    <section class="top">
        <div class="top__bg"
        <?php if($fields['verhnij_blok']['izobrazhenie']): ?>
            style="background: url(<?php echo $fields['verhnij_blok']['izobrazhenie'];?>)no-repeat center center;
		           background-size: cover;"
        <?php endif; ?>
        >
            <?php if($fields['verhnij_blok']['nazvanie_knopki'] && $fields['verhnij_blok']['ssylka']): ?>
            <div class="top__text">
              <?php if($fields['nashi_preimushhestva_teksta_v_shapke']): ?>
                <?php $pieces = explode("\r\n", $fields['nashi_preimushhestva_teksta_v_shapke']); ?>
                <?php foreach ($pieces as $key => $piece): ?>
                <?php $class = ($key % 2 === 0) ? ' slideInRight' : ' slideInLeft';?>
                    <div class="top__random-text wow <?php echo $class;?>"
                         data-wow-offset="10"
                         data-wow-iteration="1"><?php echo $piece;?>
                    </div>
                 <?php endforeach; ?>
              <?php endif; ?>
                <a href="<?php echo $fields['verhnij_blok']['ssylka'];?>"
                   class="top__text-link button-green scale callback-button-modal">
                    <?php echo $fields['verhnij_blok']['nazvanie_knopki'];?>
                </a>
            </div>
            <?php endif; ?>
        </div>
    </section>
    <section class="service container">
        <div class="service__list">
            <?php if($fields['gruppovoj']) :?>
                <div class="service__list-item wow fadeIn">
                    <?php if($fields['gruppovoj']['zagolovok'] ): ?>
                        <div class="service__list-item-title">
                            <?php echo $fields['gruppovoj']['zagolovok'] ?>
                        </div>
                    <?php endif; ?>
                    <?php if($fields['gruppovoj']['tekst'] ): ?>
                        <div class="service__list-item-text">
                            <?php echo $fields['gruppovoj']['tekst'] ?>
                        </div>
                    <?php endif; ?>
                    <div class="service__list-item-buttons">
                        <?php if($fields['gruppovoj']['knopka_1']): ?>
                            <a href="<?php echo $fields['gruppovoj']['knopka_1']['ssylka'] ?>"
                               class="button-green scale">
                                <?php echo $fields['gruppovoj']['knopka_1']['nazvanie'] ?>
                            </a>
                        <?php endif; ?>
                        <?php if($fields['gruppovoj']['knopka_2']): ?>
                            <a href="<?php echo $fields['gruppovoj']['knopka_2']['ssylka'] ?>"
                               class="button-green scale">
                                <?php echo $fields['gruppovoj']['knopka_2']['nazvanie'] ?>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php if($fields['individualnyj']) :?>
                <div class="service__list-item wow fadeIn">
                    <?php if($fields['individualnyj']['zagolovok'] ): ?>
                        <div class="service__list-item-title">
                            <?php echo $fields['individualnyj']['zagolovok'] ?>
                        </div>
                    <?php endif; ?>
                    <?php if($fields['individualnyj']['tekst'] ): ?>
                        <div class="service__list-item-text">
                            <?php echo $fields['individualnyj']['tekst'] ?>
                        </div>
                    <?php endif; ?>
                    <div class="service__list-item-buttons">
                        <?php if($fields['individualnyj']['knopka_1']): ?>
                            <a href="<?php echo $fields['individualnyj']['knopka_1']['ssylka'] ?>"
                               class="button-green scale">
                                <?php echo $fields['individualnyj']['knopka_1']['nazvanie'] ?>
                            </a>
                        <?php endif; ?>
                        <?php if($fields['individualnyj']['knopka_2']): ?>
                            <a href="<?php echo $fields['individualnyj']['knopka_2']['ssylka'] ?>"
                               class="button-green scale">
                                <?php echo $fields['individualnyj']['knopka_2']['nazvanie'] ?>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </section>

    <?php if($fields['nashi_preimucshestva']): ?>
        <section class="advantages container">
        <?php if($fields['nashi_preimucshestva']['name']): ?>
            <div class="advantages__title title"><?php echo $fields['nashi_preimucshestva']['name'] ?></div>
        <?php endif; ?>
        <div class="advantages__list">
            <?php if($fields['nashi_preimucshestva']['block_1']) :?>
                <div class="advantages__list-item wow fadeIn">
                    <?php if($fields['nashi_preimucshestva']['block_1']['ikonka']) :?>
                        <div class="advantages__list-item-icon">
                            <?php echo $fields['nashi_preimucshestva']['block_1']['ikonka'] ?>
                        </div>
                    <?php endif; ?>
                    <?php if($fields['nashi_preimucshestva']['block_1']['name']) :?>
                        <div class="advantages__list-item-title">
                            <?php echo $fields['nashi_preimucshestva']['block_1']['name'] ?>
                        </div>
                    <?php endif; ?>
                    <?php if($fields['nashi_preimucshestva']['block_1']['tekst']) :?>
                        <div class="advantages__list-item-text">
                            <?php echo $fields['nashi_preimucshestva']['block_1']['tekst'] ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>

            <?php if($fields['nashi_preimucshestva']['block_2']) :?>
                <div class="advantages__list-item wow fadeIn" data-wow-delay="0.1s">
                    <?php if($fields['nashi_preimucshestva']['block_2']['ikonka']) :?>
                        <div class="advantages__list-item-icon">
                            <?php echo $fields['nashi_preimucshestva']['block_2']['ikonka'] ?>
                        </div>
                    <?php endif; ?>
                    <?php if($fields['nashi_preimucshestva']['block_2']['name']) :?>
                        <div class="advantages__list-item-title">
                            <?php echo $fields['nashi_preimucshestva']['block_2']['name'] ?>
                        </div>
                    <?php endif; ?>
                    <?php if($fields['nashi_preimucshestva']['block_2']['tekst']) :?>
                        <div class="advantages__list-item-text">
                            <?php echo $fields['nashi_preimucshestva']['block_2']['tekst'] ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>

            <?php if($fields['nashi_preimucshestva']['block_3']) :?>
                <div class="advantages__list-item wow fadeIn" data-wow-delay="0.3s">
                    <?php if($fields['nashi_preimucshestva']['block_3']['ikonka']) :?>
                        <div class="advantages__list-item-icon">
                            <?php echo $fields['nashi_preimucshestva']['block_3']['ikonka'] ?>
                        </div>
                    <?php endif; ?>
                    <?php if($fields['nashi_preimucshestva']['block_3']['name']) :?>
                        <div class="advantages__list-item-title">
                            <?php echo $fields['nashi_preimucshestva']['block_3']['name'] ?>
                        </div>
                    <?php endif; ?>
                    <?php if($fields['nashi_preimucshestva']['block_3']['tekst']) :?>
                        <div class="advantages__list-item-text">
                            <?php echo $fields['nashi_preimucshestva']['block_3']['tekst'] ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>

            <?php if($fields['nashi_preimucshestva']['block_4']) :?>
                <div class="advantages__list-item wow fadeIn" data-wow-delay="0.4s">
                    <?php if($fields['nashi_preimucshestva']['block_4']['ikonka']) :?>
                        <div class="advantages__list-item-icon">
                            <?php echo $fields['nashi_preimucshestva']['block_4']['ikonka'] ?>
                        </div>
                    <?php endif; ?>
                    <?php if($fields['nashi_preimucshestva']['block_4']['name']) :?>
                        <div class="advantages__list-item-title">
                            <?php echo $fields['nashi_preimucshestva']['block_4']['name'] ?>
                        </div>
                    <?php endif; ?>
                    <?php if($fields['nashi_preimucshestva']['block_4']['tekst']) :?>
                        <div class="advantages__list-item-text">
                            <?php echo $fields['nashi_preimucshestva']['block_4']['tekst'] ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>

            <?php if($fields['nashi_preimucshestva']['block_5']) :?>
                <div class="advantages__list-item wow fadeIn">
                    <?php if($fields['nashi_preimucshestva']['block_5']['ikonka']) :?>
                        <div class="advantages__list-item-icon">
                            <?php echo $fields['nashi_preimucshestva']['block_5']['ikonka'] ?>
                        </div>
                    <?php endif; ?>
                    <?php if($fields['nashi_preimucshestva']['block_5']['name']) :?>
                        <div class="advantages__list-item-title">
                            <?php echo $fields['nashi_preimucshestva']['block_5']['name'] ?>
                        </div>
                    <?php endif; ?>
                    <?php if($fields['nashi_preimucshestva']['block_5']['tekst']) :?>
                        <div class="advantages__list-item-text">
                            <?php echo $fields['nashi_preimucshestva']['block_5']['tekst'] ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>

            <?php if($fields['nashi_preimucshestva']['block_6']) :?>
                <div class="advantages__list-item wow fadeIn" data-wow-delay="0.1s">
                    <?php if($fields['nashi_preimucshestva']['block_6']['ikonka']) :?>
                        <div class="advantages__list-item-icon">
                            <?php echo $fields['nashi_preimucshestva']['block_6']['ikonka'] ?>
                        </div>
                    <?php endif; ?>
                    <?php if($fields['nashi_preimucshestva']['block_6']['name']) :?>
                        <div class="advantages__list-item-title">
                            <?php echo $fields['nashi_preimucshestva']['block_6']['name'] ?>
                        </div>
                    <?php endif; ?>
                    <?php if($fields['nashi_preimucshestva']['block_6']['tekst']) :?>
                        <div class="advantages__list-item-text">
                            <?php echo $fields['nashi_preimucshestva']['block_6']['tekst'] ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>

            <?php if($fields['nashi_preimucshestva']['block_7']) :?>
                <div class="advantages__list-item wow fadeIn" data-wow-delay="0.2s">
                    <?php if($fields['nashi_preimucshestva']['block_7']['ikonka']) :?>
                        <div class="advantages__list-item-icon">
                            <?php echo $fields['nashi_preimucshestva']['block_7']['ikonka'] ?>
                        </div>
                    <?php endif; ?>
                    <?php if($fields['nashi_preimucshestva']['block_7']['name']) :?>
                        <div class="advantages__list-item-title">
                            <?php echo $fields['nashi_preimucshestva']['block_7']['name'] ?>
                        </div>
                    <?php endif; ?>
                    <?php if($fields['nashi_preimucshestva']['block_7']['tekst']) :?>
                        <div class="advantages__list-item-text">
                            <?php echo $fields['nashi_preimucshestva']['block_7']['tekst'] ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>

            <?php if($fields['nashi_preimucshestva']['block_8']) :?>
                <div class="advantages__list-item wow fadeIn" data-wow-delay="0.3s">
                    <?php if($fields['nashi_preimucshestva']['block_8']['ikonka']) :?>
                        <div class="advantages__list-item-icon">
                            <?php echo $fields['nashi_preimucshestva']['block_8']['ikonka'] ?>
                        </div>
                    <?php endif; ?>
                    <?php if($fields['nashi_preimucshestva']['block_8']['name']) :?>
                        <div class="advantages__list-item-title">
                            <?php echo $fields['nashi_preimucshestva']['block_8']['name'] ?>
                        </div>
                    <?php endif; ?>
                    <?php if($fields['nashi_preimucshestva']['block_8']['tekst']) :?>
                        <div class="advantages__list-item-text">
                            <?php echo $fields['nashi_preimucshestva']['block_8']['tekst'] ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        </div>
    </section>
    <?php endif; ?>

    <?php if($fields['stoimost_transfera']): ?>
        <section class="transfer-price container">
            <div class="transfer-price__content">
                <?php if($fields['stoimost_transfera']['name']) :?>
                    <div class="transfer-price__title title">
                        <?php echo $fields['stoimost_transfera']['name']?>
                    </div>
                <?php endif; ?>
                <div class="transfer-price__list">
                    <?php if($fields['stoimost_transfera']['block_1']) :?>
                        <div class="transfer-price__list-item wow fadeIn">
                            <?php if($fields['stoimost_transfera']['block_1']['izobrazhenie']) :?>
                                <div class="transfer-price__list-item-img">
                                    <?php echo wp_get_attachment_image(
                                        $fields['stoimost_transfera']['block_1']['izobrazhenie'],
                                        'medium'
                                    ) ?>
                                </div>
                            <?php endif; ?>
                            <?php if($fields['stoimost_transfera']['block_1']['tekst']) :?>
                                <div class="transfer-price__list-item-title">
                                    <?php echo $fields['stoimost_transfera']['block_1']['tekst'] ?>
                                </div>
                            <?php endif; ?>
                            <?php if($fields['stoimost_transfera']['block_1']['stoimost']) :?>
                                <div class="transfer-price__list-item-text">
                                    <?php echo $fields['stoimost_transfera']['block_1']['stoimost'] ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                    <?php if($fields['stoimost_transfera']['block_2']) :?>
                        <div class="transfer-price__list-item wow fadeIn" data-wow-delay="0.1s">
                            <?php if($fields['stoimost_transfera']['block_2']['izobrazhenie']) :?>
                                <div class="transfer-price__list-item-img">
                                    <?php echo wp_get_attachment_image(
                                        $fields['stoimost_transfera']['block_2']['izobrazhenie'],
                                        'medium'
                                    ) ?>
                                </div>
                            <?php endif; ?>
                            <?php if($fields['stoimost_transfera']['block_2']['tekst']) :?>
                                <div class="transfer-price__list-item-title">
                                    <?php echo $fields['stoimost_transfera']['block_2']['tekst'] ?>
                                </div>
                            <?php endif; ?>
                            <?php if($fields['stoimost_transfera']['block_2']['stoimost']) :?>
                                <div class="transfer-price__list-item-text">
                                    <?php echo $fields['stoimost_transfera']['block_2']['stoimost'] ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                    <?php if($fields['stoimost_transfera']['block_3']) :?>
                        <div class="transfer-price__list-item wow fadeIn" data-wow-delay="0.3s">
                            <?php if($fields['stoimost_transfera']['block_3']['izobrazhenie']) :?>
                                <div class="transfer-price__list-item-img">
                                    <?php echo wp_get_attachment_image(
                                        $fields['stoimost_transfera']['block_3']['izobrazhenie'],
                                        'medium'
                                    ) ?>
                                </div>
                            <?php endif; ?>
                            <?php if($fields['stoimost_transfera']['block_3']['tekst']) :?>
                                <div class="transfer-price__list-item-title">
                                    <?php echo $fields['stoimost_transfera']['block_3']['tekst'] ?>
                                </div>
                            <?php endif; ?>
                            <?php if($fields['stoimost_transfera']['block_3']['stoimost']) :?>
                                <div class="transfer-price__list-item-text">
                                    <?php echo $fields['stoimost_transfera']['block_3']['stoimost'] ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                </div>
                <?php if($fields['stoimost_transfera']['nazvanie_knopki'] &&
                $fields['stoimost_transfera']['ssylka'] ) :?>
                    <div class="transfer-price__button">
                        <a href="<?php echo $fields['stoimost_transfera']['ssylka'] ?>" class="button-green scale no-margin right">
                            <?php echo $fields['stoimost_transfera']['nazvanie_knopki'] ?>
                        </a>
                    </div>
                <?php endif; ?>
            </div>
        </section>
    <?php endif; ?>

    <section class="content container">
        <h1 class="content__title"><?php the_title();?></h1>
        <?php
            the_post();
            the_content();
        ?>
         <div class="content__button">
            <a href="/o-kompanii/" class="button-green scale no-margin right">Подробнее</a>
        </div>
    </section>

    <?php $reviews = get_posts( array(
        'numberposts' => 3,
        'category'    => 0,
        'orderby'     => 'date',
        'order'       => 'DESC',
        'post_type'   => 'reviews',
    )); ?>

    <?php if($reviews): ?>
    <section class="people-says container">
        <div class="people-says__title title">Отзывы</div>
        <div class="people-says__list">
            <?php foreach( $reviews as $post ) : setup_postdata( $post );?>
                <div class="people-says__item">
                    <div class="people-says__item-img" style="background-image: url('<?php echo get_the_post_thumbnail_url('', 'medium_large');?>')"></div>
                    <div class="people-says__item-content">
                        <div class="people-says__item-title">
                            <?php the_title();?>
                            <span class="date">
                                <?php echo get_the_date('n-j-Y');?>
                            </span>
                        </div>
                        <div class="people-says__item-text">
                            <?php the_content();?>
                        </div>
                    </div>
                </div>
            <?php endforeach; wp_reset_postdata(); ?>
        </div>
        <div class="people-says__button">
            <a href="/reviews/" class="button-green scale no-margin">Все отзывы</a>
        </div>
    </section>
    <?php endif; ?>

    <?php if($fields['zakazhite_transfer']): ?>
    <section class="order container-vertical"
        <?php if($fields['zakazhite_transfer']['izobrazhenie']): ?>
            style="background-image: url(<?php echo $fields['zakazhite_transfer']['izobrazhenie'];?>);
                   background-size: cover;
                   background-attachment: fixed;
                   background-position: center;"
        <?php endif; ?>
    >
        <div class="order__content">
            <?php if($fields['zakazhite_transfer']['tekst']): ?>
                <div class="order__content-text">
                    <?php echo $fields['zakazhite_transfer']['tekst'];?>
                </div>
            <?php endif; ?>
            <?php if($fields['zakazhite_transfer']['nazvanie_knopki'] &&
                    $fields['zakazhite_transfer']['ssylka']): ?>
                <div class="order__button">
                    <a href="<?php echo $fields['zakazhite_transfer']['ssylka']?>"
                       class="button-green scale no-margin">
                        <?php echo $fields['zakazhite_transfer']['nazvanie_knopki']?>
                    </a>
                </div>
            <?php endif; ?>
        </div>
    </section>
    <?php endif; ?>

    <?php if($fields['ostalis_voprosy']): ?>
        <section class="questions container-vertical">
            <div class="questions__content">
                <?php if($fields['ostalis_voprosy']['tekst']): ?>
                    <div class="questions__content-text">
                        <?php echo $fields['ostalis_voprosy']['tekst'];?>
                    </div>
                <?php endif; ?>
                <?php if($fields['ostalis_voprosy']['nomer_telefona']): ?>
                    <div class="questions__content-phone">
                        <a class="questions__content-phone-link scale"
                           href="tel:<?php echo $fields['ostalis_voprosy']['nomer_telefona'];?>">
                            <?php echo $fields['ostalis_voprosy']['nomer_telefona'];?>
                        </a>
                    </div>
                <?php endif; ?>
            </div>
        </section>
    <?php endif; ?>

    <?php $partners = get_posts( array(
        'numberposts' => 10,
        'category'    => 0,
        'orderby'     => 'date',
        'order'       => 'DESC',
        'post_type'   => 'partners',
    ));  ?>
    <?php if($partners): ?>
        <section class="partners container">
            <div class="partners__content">
                <div class="partners__title title">Наши партнеры</div>
                <div class="center slider">
                    <?php foreach( $partners as $post ) : setup_postdata( $post );?>
                        <?php if(has_post_thumbnail($post)):?>
                            <div class="partners__item">
                                <img src="<?php echo the_post_thumbnail_url( 'medium' ); ?>">
                            </div>
                        <?php endif; ?>
                    <?php endforeach;?>
                </div>
                <div class="partners__button">
                    <a href="/partners/" class="button-green scale no-margin right">Подробнее</a>
                </div>
            </div>
        </section>
    <?php endif; ?>
</div>
<!-- .homepage -->

<?php get_footer(); ?>
