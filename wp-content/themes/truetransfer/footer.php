<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 */
?>

		</div><!-- .site-content -->

		<footer class="footer">
            <div class="footer__content">
                <div class="footer__content-item">
                    <div class="footer__content-subitems">
                        <?php if(get_option('show_ya_review')) :?>
                            <div class="footer__content-subitem">
                                <iframe src="https://yandex.ru/sprav/widget/rating-badge/217644061247?type=rating" width="150" height="50" frameborder="0"></iframe>
                            </div>
                        <?php endif;?>
                        <?php if(get_option('vk_url')) :?>
                            <div class="footer__content-subitem">
                                <a class="footer__content-item-link icon"
                                   href="<?php echo get_option('vk_url')?>"
                                   target="_blank"
                                >
                                    <i class="fab fa-vk fa-2x"></i>
                                </a>
                            </div>
                        <?php endif;?>
                        <?php if(get_option('facebook_url')) :?>
                            <div class="footer__content-subitem">
                                <a class="footer__content-item-link icon"
                                   href="<?php echo get_option('facebook_url')?>"
                                   target="_blank"
                                >
                                    <i class="fab fa-facebook-square fa-2x"></i>
                                </a>
                            </div>
                        <?php endif;?>
                    </div>
                </div>
                <div class="footer__content-item">
                    <?php echo get_option('privacy');?>
                </div>
                <div class="footer__content-item">
                    <?php echo get_option('copyright');?>
                </div>
            </div>
		</footer><!-- .site-footer -->
	</div><!-- .site-inner -->
</div><!-- .site -->
<a id="button"><i class="fas fa-arrow-up"></i></a>
<?php if (get_option('show_call_button')): ?>
    <div type="button" class="callback-button-modal callback-button scale">
        <i class="fa fa-phone"></i>
    </div>
<?php endif;?>
<!--<link href="https://use.fontawesome.com/releases/v5.10.1/css/all.css?v=1.1" rel="stylesheet">-->
<?php wp_footer(); ?>
<script src="<?php bloginfo('template_url'); ?>/dist/js/scripts.min.js?v=1.1"></script>
<?php echo get_option('custom_footer_code')?>
<div style="display: none" class="tablePrice">
    <?php echo getPostContentFooter();?>
</div>
<script>
    jQuery('.gallery-item a').colorbox({
        rel:'gal',
        fixed: true,
        maxHeight: '75%',
        maxWidth: '100%',
        opacity: "0.35",
        closeButton: true,
        reposition: true
    });
    <?php if (get_field('cf7_redirect')): ?>
    document.addEventListener( 'wpcf7mailsent', function( event ) {
        setTimeout( () => {
            location = '<?php echo get_field("redirect_to")?>';
        }, 2000 );
    }, false );
    <?php endif;?>
</script>
<?php if (get_field( 'widget_button' )): ?>
<style>
    .rncb-widget-form {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-between;
        align-items: start;
        flex-direction: column;
    }
    .rncb-widget-field {
        outline: 0;
        border: 1px solid #723098;
        padding: 5px 10px;
        color: inherit;
        font: inherit;
        font-size: 18px;
        line-height: 26px;
        width: 175px;
    }
    .rncb-form-element button.custom-button {
        padding: 10px 20px;
        display: inline-block;
        margin: 10px 0 0;
        width: 175px;
    }
    .break {
        flex-basis: 100%;
        height: 0;
    }
</style>
<script>


</script>
<script src="https://mpi.rncb.ru:8444/widget/script.js?wId=flex"></script>
<script>
    const observer = new MutationObserver(function (mutations) {
        const iframe = document.querySelector("iframe[name='hppIFrame']");
        if (iframe) {
            iframe.style.removeProperty('display');
            observer.disconnect();
        }
    });

    observer.observe(document, {attributes: false, childList: true, characterData: false, subtree: true});

    window.addEventListener('DOMContentLoaded', (event) => {
        document.getElementById('initWidget').addEventListener('click', function(el) {
            const orderData = {
                'order': {
                    "typeRid": "Sale Order Type_Single_CVV",
                    "amount": document.querySelector('#fld_amount').value.toString(),
                    "currency": "RUB",
                    "description": "",
                    "language": "ru",
                    "hppRedirectUrl": location.href
                },
                'auth': {
                    'userName': 'TerminalSys/50000435',
                    'password': 'Apr@03042024',
                },
                'terminalId': '1785',
                'merchantId': '1725'
            };

            if (false) {
                orderData.order.description = orderData.order.description.substr(0, 140);
            }

            const listeners = {
                onSuccess(data) {
                    console.log('Заказ оплачен', data);
                },
                onFail(data, err) {
                    console.log('Произошла ошибка', data, err);
                }
            };

            const hppIntegrationApi = new HppIntegrationApi(orderData, listeners);
            const payButton = document.getElementById('continueButton');

            hppIntegrationApi.createWidget(document.getElementById('widgetContainer'), {payButton});
            payButton.click();

        });
    });
</script>
<?php endif;?>
</body>
</html>
