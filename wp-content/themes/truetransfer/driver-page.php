<?php
/**
 * Template Name: Для водителей
 *
 * The template for displaying homepage
 */

get_header(); ?>
    <section class="page container">
        <div class="page__content">
            <h1 class="page__title title"><?php the_title();?></h1>
            <div class="page__text">
                <?php the_content();?>
                <?php $fields = get_fields();
                if($fields['dlya_voditelej']): ?>
                    <div class="for-drivers">
                        <div class="for-drivers__list">
                            <div class="for-drivers__list-item">
                                <?php echo $fields['dlya_voditelej']['so_svoim_avto'];?>
                            </div>
                            <div class="for-drivers__list-item">
                                <?php echo $fields['dlya_voditelej']['na_nashem_avto'];?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </section>
<?php get_footer(); ?>