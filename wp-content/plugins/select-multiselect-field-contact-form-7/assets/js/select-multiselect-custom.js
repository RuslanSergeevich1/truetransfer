/*Select and Multi-Select Custom jQuery Events*/
jQuery('.wpcf7').ready(function(){
    jQuery('select.wpcf7-selct-multiselct').select2({
        language: "ru"
    });
    jQuery('select.wpcf7-selct-multiselct').val(null).trigger('change');
});

jQuery(".wpcf7").on( 'wpcf7:mailsent', function( event ){
    jQuery('select.wpcf7-selct-multiselct').val(null).trigger('change');
    jQuery('.transfer-to .select2-selection__placeholder').text('Выберите направление');
});

jQuery.fn.select2.amd.define('select2/i18n/ru',[],function () {
    // Russian
    return {
        errorLoading: function () {
            return 'Результат не может быть загружен.';
        },
        inputTooLong: function (args) {
            var overChars = args.input.length - args.maximum;
            var message = 'Пожалуйста, удалите ' + overChars + ' символ';
            if (overChars >= 2 && overChars <= 4) {
                message += 'а';
            } else if (overChars >= 5) {
                message += 'ов';
            }
            return message;
        },
        inputTooShort: function (args) {
            var remainingChars = args.minimum - args.input.length;

            var message = 'Пожалуйста, введите ' + remainingChars + ' или более символов';

            return message;
        },
        loadingMore: function () {
            return 'Загружаем ещё ресурсы…';
        },
        maximumSelected: function (args) {
            var message = 'Вы можете выбрать ' + args.maximum + ' элемент';

            if (args.maximum  >= 2 && args.maximum <= 4) {
                message += 'а';
            } else if (args.maximum >= 5) {
                message += 'ов';
            }

            return message;
        },
        noResults: function () {
            return 'Ничего не найдено';
        },
        searching: function () {
            return 'Поиск…';
        }
    };
});